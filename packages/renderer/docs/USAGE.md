# Renderer

## Try it out

Interact with a [live demo of the @NAME@ component](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).

## Installation

```sh
npm install @NAME@
```

## Using the component

Use the component in your React app as follows:

```
import Renderer from '@NAME@';
ReactDOM.render(<Renderer doc={DOCUMENT} />, container);
```
