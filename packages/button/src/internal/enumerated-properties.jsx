export const appearance = {
  values: [
    'primary',
    'default',
    'subtle',
    'link',
    'subtle-link',
  ],
  default: 'default',
};

export const type = {
  values: ['button', 'submit'],
  default: 'button',
};

export const spacing = {
  values: ['default', 'compact', 'none'],
  default: 'default',
};

export const theme = {
  values: ['default', 'dark'],
  default: 'default',
};
