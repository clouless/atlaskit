import createError from 'create-error';

 // eslint-disable-next-line import/prefer-default-export
export const NotImplementedError = createError('NotImplementedError');
