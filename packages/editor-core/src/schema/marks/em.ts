import { MarkSpec, marks } from '../../prosemirror';

export const em: MarkSpec = {...marks.em,
  inclusive: true
};
