import { MarkSpec, marks } from '../../prosemirror';

export const strong: MarkSpec = {
  ...marks.strong,
  inclusive: true
};
