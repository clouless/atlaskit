import { style } from 'typestyle';

export const seperator = style({
  background: 'grey',
  width: 1,
  height: 20,
  display: 'inline-block',
  margin: '0 10px'
});

export const container = style({
  display: 'flex',
  alignItems: 'center',
  padding: '5px 10px'
});
