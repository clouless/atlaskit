import { style } from 'typestyle';

export const buttonContent = style({
  width: '80px',
});

export const container = style({
  position: 'relative',
  width: '92px',
});
