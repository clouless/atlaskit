/* tslint:disable:variable-name */
import styled from 'styled-components';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

export const Container = styled.div`
  width: ${akGridSizeUnitless * 32}px
`;

export const SliderContainer = styled.div`
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: row;
  margin-top: ${akGridSizeUnitless}px;
`;
