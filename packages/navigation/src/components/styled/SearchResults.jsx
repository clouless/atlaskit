import styled from 'styled-components';
import {
    akGridSizeUnitless,
 } from '@atlaskit/util-shared-styles';

const SearchResults = styled.div`
  margin-top: ${akGridSizeUnitless * 3}px;
`;

SearchResults.displayName = 'SearchResults';
export default SearchResults;
