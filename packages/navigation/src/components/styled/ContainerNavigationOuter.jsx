import styled from 'styled-components';

const ContainerNavigationOuter = styled.div`
  height: 100%;
  position: fixed;
  width: inherit;
`;

ContainerNavigationOuter.displayName = 'ContainerNavigationOuter';

export default ContainerNavigationOuter;
