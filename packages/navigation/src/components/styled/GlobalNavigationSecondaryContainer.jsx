import styled from 'styled-components';

const GlobalNavigationSecondaryContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

GlobalNavigationSecondaryContainer.displayName = 'GlobalNavigationSecondaryContainer';
export default GlobalNavigationSecondaryContainer;
