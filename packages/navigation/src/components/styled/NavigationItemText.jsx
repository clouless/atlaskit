import styled from 'styled-components';

const NavigationItemText = styled.div`
  overflow-x: hidden;
  display: inline-block;
  flex-grow: 1;
`;

NavigationItemText.displayName = 'NavigationItemText';
export default NavigationItemText;
