import {
  akBorderRadius,
  akColorN40,
} from '@atlaskit/util-shared-styles';

export const noDialogContainerBorderColor = akColorN40; // This has not been confirmed by the ADG yet
export const noDialogContainerBorderRadius = akBorderRadius;
export const noDialogContainerBoxShadow = '0 3px 6px rgba(0, 0, 0, 0.2)';

export const scrollableMaxHeight = '264px';
export const mentionListWidth = '340px';
